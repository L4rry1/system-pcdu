# Power Conditioning and Distribution Unit (PCDU)

This is a prototype of a power conditioning and distribution unit (PCDU) that
implements a full redundancy scheme and provides a CAN bus interface according
to the LibreCube board specification. The main features are:

- Provide input connectors for up to six solar cells and condition their input energy
- Provide connector for Li-Ion battery (2 in series) and manage battery charging (when input from solar cells exceeds consumer load) and discharge (when no input from solar cells available)
- Provide a main power switch that disconnects all electrical loads from solar cell input and batteries
- Provide measurements of current, voltage, and temperature at selected points
- Provide stable 5 Volt output power to attached loads
- Provide configurable output switches (permanent ON or switchable ON/OFF)
- Provide two cold-redundant telecommand and telemetry interfaces (TMTC I/F) using CAN Bus

![](docs/pcdu.png)

## Getting Started

Find in the *src* folder the KiCAD source files of the board.

In the *tests* folder are the individual modules (such as battery charge module) for unit level testing.

## Documentation

See the *docs* folder for references and datasheets.

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/system-pcdu/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/system-pcdu

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the CERN Open Hardware license. See the [LICENSE](./LICENSE.txt) file for details.
