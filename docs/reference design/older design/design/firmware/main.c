/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 * Last update: 2014-11-18 
 */

/*	Summary:
 *		The firmware runs a main loop that checks for received commands
 *		from a I2C master and collects periodically housekeeping data.
 *		The I2C master can enable/disable battery charing, change settings,
 *		and trigger a reset of the microcontroller.
 *
 *	To do:
 *		- implement watchdog timer
 *		- implement I2C timeout 
 */

/* Includes */
#include "definitions.h"
#include "modules/c8051f310.h"
#include "functions/common.h"
#include "modules/i2c.h"

/* Global variables */
Register_t reg;
uint8_t* regPtr = &reg.null;
uint8_t targetReg = 0;
uint8_t targetRegFlag = 0;
uint8_t value = 0;
uint8_t valueFlag = 0;
uint32_t timerHk = 0;

void main(void)
{
    InitModules(); /* Configure and initialize hardware */
    InitRegister(); /* Load default register values */
    
    /* Do the housekeeping measurements at boot */
    reg.flag.busy = 1;
    reg.flag.complete = 0;
    TriggerMeasurements();
    reg.flag.complete = 1;
    reg.flag.busy = 0;
    
    for (;;)
    {
        /* Execute a received command from I2C master */
        if (targetRegFlag && valueFlag)
        {
            RegWrite(targetReg, value);
            targetRegFlag = 0;
            valueFlag = 0;
        }
        /* Refresh housekeeping data, if timer expired */
        if ((reg.hkperiod != 0) && (timerHk >= reg.hkperiod))
        {
            timerHk = 0; /* Reset timer count */
            reg.flag.busy = 1;
            reg.flag.complete = 0;
            TriggerMeasurements();
            reg.flag.complete = 1;
            reg.flag.busy = 0;
        }
    }
}

void ResetIsr(void) __interrupt 0 /* Triggers when CBS_RESET line goes low */
{
    static uint8_t i = 0;
    if (i > 0) /* This counter is used to avoid a reset at bootup */
    {
        MicrocontrollerReset();
    }
    i++;
}

void Timer2Isr(void) __interrupt 5 /* Timer for housekeeping refresh period */
{
    timerHk++; /* increases every 0.1 sec */
    TF2H = 0; /* clear timer 2 interrupt flag (necessary) */
}

void I2cIsr(void) __interrupt 7 /* I2C ISR, with highest priority */
{
    if (!ARBLOST)
    {
        switch (SMB0CN & 0xF0)
        // Decode the SMBus status vector   
        {
            // Slave Receiver: Start+Address received   
            case SMB_SRADD:

                STA = 0;           // Clear STA bit   
                if ((SMB0DAT >> 1) == I2C_ADR)           // Decode address   
                {                         // If the received address matches,   
                    ACK = 1;                // ACK the received slave address   
                    if ((SMB0DAT & 0x01) == I2C_WRITE)    // If the transfer is a master WRITE,   
                    {
                        targetRegFlag = 0; /* clear flags at start */
                        valueFlag = 0;
                    }
                    else
                    {
                        SMB0DAT = regPtr[targetReg]; /* Send value of register */
                    }
                }
                else                    // If received slave address does not   
                {                          // match,   
                    ACK = 0;                         // NACK received address   
                }
                break;
                
                // Slave Receiver: Data received   
            case SMB_SRDB:

                if (!targetRegFlag) /* If this is the first byte */
                {
                    targetRegFlag = 1;
                    targetReg = SMB0DAT;
                }
                else if (ACK) /* Otherwise load second byte (only if previous was ACK) */
                {
                    valueFlag = 1; /* Set flag */
                    value = SMB0DAT; /* Load value */
                }
                ACK = 1;                   // ACK received data   
                
                break;
                
                // Slave Receiver: Stop received while either a Slave Receiver or   
                // Slave Transmitter   
            case SMB_SRSTO:

                STO = 0;            // STO must be cleared by software when   
                                    // a STOP is detected as a slave   
                break;
                
                // Slave Transmitter: Data byte transmitted   
            case SMB_STDB:
                // No action required;   
                // one-byte transfers   
                // only for this example   
                break;
                
                // Slave Transmitter: Arbitration lost, Stop detected   
                //   
                // This state will only be entered on a bus error condition.   
                // In normal operation, the slave is no longer sending data or has   
                // data pending when a STOP is received from the master, so the TXMODE   
                // bit is cleared and the slave goes to the SRSTO state.   
            case SMB_STSTO:

                STO = 0; // STO must be cleared by software when a STOP is detected as a slave   
                break;
                
                // Default: all other cases undefined   
            default:

                SMB0CF &= ~0x80; // Reset communication   
                SMB0CF |= 0x80;
                STA = 0;
                STO = 0;
                ACK = 0;
                break;
        }
    }
    // ARBLOST = 1, Abort failed transfer   
    else
    {
        STA = 0;
        STO = 0;
        ACK = 0;
    }
    
    SI = 0; // Clear SMBus interrupt flag   
}

