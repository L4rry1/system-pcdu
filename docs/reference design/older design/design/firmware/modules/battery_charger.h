/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 */

#ifndef	BATTERY_CHARGER_H
#define BATTERY_CHARGER_H

/* Includes */
#include "../definitions.h" 
#include "microcontroller.h"

/* Function declarations */
void BatteryChargerState(const uint8_t state);
uint8_t BatteryChargerStatus(void);

#endif
