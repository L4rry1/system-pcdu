/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 */ 

#include "microcontroller.h"

void Reset_Sources_Init()
{
    uint16_t i = 0;
    VDM0CN    = 0x80;
    for (i = 0; i < 350; i++);  /* Wait 100us for initialization */
    RSTSRC    = 0x02;
}

void PCA_Init()
{
    PCA0MD    &= ~0x40;
    PCA0MD    = 0x00;
}

void Timer_Init()
{
    TCON      = 0x11;
    TMOD      = 0x22;
    CKCON     = 0x06;
    TH0       = 0xF6;
    TH1       = 0x61;
    TMR2CN    = 0x04;
    TMR2RLH   = 0x9A;
    TMR2H     = 0x9A;
}

void SMBus_Init()
{
    SMB0CF    = 0x80;
}

void ADC_Init()
{
    ADC0CF    = 0x00;
}

void Voltage_Reference_Init()
{
    REF0CN    = 0x06;
}

void Port_IO_Init()
{
    // P0.0  -  Skipped,     Open-Drain, Analog
    // P0.1  -  SDA (SMBus), Open-Drain, Digital
    // P0.2  -  SCL (SMBus), Open-Drain, Digital
    // P0.3  -  Unassigned,  Open-Drain, Digital
    // P0.4  -  Skipped,     Push-Pull,  Digital
    // P0.5  -  Unassigned,  Open-Drain, Digital
    // P0.6  -  Unassigned,  Open-Drain, Digital
    // P0.7  -  Unassigned,  Open-Drain, Digital

    // P1.0  -  Skipped,     Open-Drain, Analog
    // P1.1  -  Skipped,     Open-Drain, Analog
    // P1.2  -  Skipped,     Open-Drain, Analog
    // P1.3  -  Skipped,     Open-Drain, Analog
    // P1.4  -  Skipped,     Open-Drain, Analog
    // P1.5  -  Skipped,     Open-Drain, Analog
    // P1.6  -  Skipped,     Open-Drain, Analog
    // P1.7  -  Skipped,     Open-Drain, Analog
    // P2.0  -  Skipped,     Open-Drain, Analog
    // P2.1  -  Skipped,     Open-Drain, Analog
    // P2.2  -  Skipped,     Open-Drain, Analog
    // P2.3  -  Skipped,     Open-Drain, Analog

    P0MDIN    = 0xFE;
    P1MDIN    = 0x00;
    P2MDIN    = 0x80;
    P0MDOUT   = 0x10;
    P0SKIP    = 0x11;
    P1SKIP    = 0xFF;
    P2SKIP    = 0x0F;
    XBR0      = 0x04;
    XBR1      = 0x40;

	P0	= 0xFF;
}

void Interrupts_Init()
{
    IP        = 0x01;
    EIE1      = 0x01;
    EIP1      = 0x01;
    IT01CF    = 0x04;
    IE        = 0xA1;
}

void MicrocontrollerInit(void) 
{
    PCA_Init();					// disable WDT
	Reset_Sources_Init();		// vdd monitor
								// 3.0625 MHz
    Timer_Init();				// timer0 for i2c
    SMBus_Init();				// 100 kHz max.
    ADC_Init();					// same speed as mcu
    Voltage_Reference_Init();	// vref, temp sensor, bias
    Port_IO_Init();				
    Interrupts_Init();			// i2c
}

void MicrocontrollerReset(void) 
{
	RSTSRC = RSTSRC | 0x10;
}

void AdcState(const uint8_t state)  
{
	if (state == ON)  
	{
		AD0EN	= 1; /* enable ADC */ 
	}
	else if (state == OFF)
	{
		AD0EN	= 0; /* set ADC in low-power shutdown */ 
	}
}

uint16_t short AdcMeasure(const uint8_t type, const uint8_t port)
{	
	int16_t buffer = 0;

	if (AD0EN) 				/* make sure that ADC is enabled */
	{																		    
		switch (type)
		{
    		case ADC_INTERNAL: 	
    		
    			AMX0P	= 0x1E;		// select temp sensor as positive input 
    			AMX0N	= 0x1F; 	// single-ended mode with GND as negative input 		
    			AD0INT	= 0; 		// clear ADC interrupt flag 
    			AD0BUSY	= 1; 		// trigger conversion 
    			while(!AD0INT); 	// wait for conversion to complete 		
    			buffer = (ADC0H & 0x03)<<8;
    			buffer = buffer + ADC0L;
    			break;

    		case ADC_EXTERNAL: 	

    			AMX0P	= port;		// select input 
    			AMX0N	= 0x1F; 	// single-ended mode with GND as negative input 		
    			AD0INT	= 0; 		// clear ADC interrupt flag 
    			AD0BUSY	= 1; 		// trigger conversion 
    			while(!AD0INT); 	// wait for conversion to complete 		
    			buffer = (ADC0H & 0x03)<<8;
    			buffer = buffer + ADC0L;
    			break;	
		}
	}

	return buffer;
}	
