/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 */

#ifndef	I2C_H
#define I2C_H

/* I2C specific */
#define  I2C_WRITE      0x00 // SMBus WRITE command  
#define  I2C_READ       0x01 // SMBus READ command   
#define  SMB_SRADD      0x20 // (SR) slave address received 
#define  SMB_SRSTO      0x10 // (SR) STOP detected while SR or ST   
#define  SMB_SRDB       0x00 // (SR) data byte received
#define  SMB_STDB       0x40 // (ST) data byte transmitted   
#define  SMB_STSTO      0x50 // (ST) STOP detected during a transaction; bus error   

#endif
