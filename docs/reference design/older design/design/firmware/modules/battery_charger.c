/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 */  
 
#include "battery_charger.h"

void BatteryChargerState(const uint8_t state)
{
	if (state == 1) BCM_EN = 1;
	else if (state == 0) BCM_EN = 0;
}

uint8_t BatteryChargerStatus(void)
{
	uint8_t status = 0;

	if (BCM_STATUS_1 == 0) status |= 0x01; /* Goes low during charging */
	if (BCM_STATUS_2 == 0) status |= 0x02; /* Goes low when 90% complete */

	return status;
}
