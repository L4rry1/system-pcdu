/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 */

#include "common.h"

/* External global variables */
extern Register_t reg;
extern uint8_t* regPtr;
extern uint32_t timerHk;

/* Load default register values */
void InitRegister(void) 
{
	uint8_t i = 0;
	
	/* Delete all entries */
	for (i = 0; i < sizeof(reg); i++) regPtr[i] = 0; 

	/* Set to default value */
	reg.hkperiod				= HK_PERIOD;
	reg.measure.temp			= 1;
	reg.measure.current 		= 1;
	reg.measure.volt			= 1;
	reg.measure.batt_status		= 1;
	reg.flag.charging_enabled 	= 1;
}

void InitModules(void) 
{
	MicrocontrollerInit();	
	BatteryChargerState(ON);
}

void RegWrite(const uint8_t targetReg, const uint8_t value)
{
	reg.flag.busy = 1;

	switch (targetReg)
	{
		case REG_CMD:
			switch (value)
			{
				case CMD_RESET:
					MicrocontrollerReset();
					break;
								
				case CMD_MEASURE:
					reg.flag.complete 	= 0;
					TriggerMeasurements(); 
					reg.flag.complete 	= 1;
					break;		

				case CMD_CHARGING_ENABLE:
					BatteryChargerState(ON);
					reg.flag.charging_enabled = 1;
					break;

				case CMD_CHARGING_DISABLE:
					BatteryChargerState(OFF);
					reg.flag.charging_enabled = 0;
					break;
										
				default:
					break;
			}
			break;

		case REG_HKPERIOD:
			reg.hkperiod = value;
			timerHk = 0;
			break;

		case REG_MEASURE:
			reg.measure.byte = value;
			break;

		default:
			break;
	}

	reg.flag.busy = 0;
}


void TriggerMeasurements(void)
{
	uint16_t buffer;

	if (reg.measure.temp)
	{
		AdcState(ON); 

		buffer = AdcMeasure(ADC_INTERNAL, 0);
		reg.mcutemp_l = (buffer & 0xFF);
		reg.mcutemp_h = ((buffer>>8) & 0xFF);

		AdcState(OFF); 
	}

	if (reg.measure.current)
	{
		AdcState(ON); 

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 0);
		reg.c1_l = (buffer & 0xFF);
		reg.c1_h = ((buffer>>8) & 0xFF);
		
		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 1);
		reg.c2_l = (buffer & 0xFF);
		reg.c2_h = ((buffer>>8) & 0xFF);

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 2);
		reg.c3_l = (buffer & 0xFF);
		reg.c3_h = ((buffer>>8) & 0xFF);
		
		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 3);
		reg.c4_l = (buffer & 0xFF);
		reg.c4_h = ((buffer>>8) & 0xFF);

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 4);
		reg.c5_l = (buffer & 0xFF);
		reg.c5_h = ((buffer>>8) & 0xFF);
		
		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 5);
		reg.c6_l = (buffer & 0xFF);
		reg.c6_h = ((buffer>>8) & 0xFF);

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 6);
		reg.c7_l = (buffer & 0xFF);
		reg.c7_h = ((buffer>>8) & 0xFF);
		
		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 7);
		reg.c8_l = (buffer & 0xFF);
		reg.c8_h = ((buffer>>8) & 0xFF);

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 8);
		reg.c9_l = (buffer & 0xFF);
		reg.c9_h = ((buffer>>8) & 0xFF);
		
		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 9);
		reg.c10_l = (buffer & 0xFF);
		reg.c10_h = ((buffer>>8) & 0xFF);

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 10);
		reg.c11_l = (buffer & 0xFF);
		reg.c11_h = ((buffer>>8) & 0xFF);
		
		buffer = AdcMeasure(ADC_EXTERNAL, ADC_CUR_PORT + 11);
		reg.c12_l = (buffer & 0xFF);
		reg.c12_h = ((buffer>>8) & 0xFF);

		AdcState(OFF); 
	}

	if (reg.measure.volt)
	{
		AdcState(ON); 

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_VOL_PORT + 0);
		reg.v1_l = (buffer & 0xFF);
		reg.v1_h = ((buffer>>8) & 0xFF);

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_VOL_PORT + 1);
		reg.v2_l = (buffer & 0xFF);
		reg.v2_h = ((buffer>>8) & 0xFF);

		buffer = AdcMeasure(ADC_EXTERNAL, ADC_VOL_PORT + 2);
		reg.v3_l = (buffer & 0xFF);
		reg.v3_h = ((buffer>>8) & 0xFF);

		AdcState(OFF); 
	}

	if (reg.measure.batt_status)
	{
		buffer = BatteryChargerStatus();
		reg.flag.charging 	= (buffer & 0x01);
		reg.flag.full 		= ((buffer>>1) & 0x01); 
	}
}
