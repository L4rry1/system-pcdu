/* ------------------------------------------
 * LibreCube LC-2201a Electrical Power System
 * ------------------------------------------
 */

#ifndef	DEFINITIONS_H
#define DEFINITIONS_H

/* Type definitions */
typedef signed char int8_t;
typedef short int int16_t;
typedef long int int32_t;
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned long int uint32_t;

/* General definitions */
#define SET						1
#define CLEAR					0
#define ON						1
#define OFF						0
#define PASS					1
#define FAIL					0

/* Configuration */
#define I2C_ADR					0x12
#define HK_PERIOD				10			/* yields 1 second refresh rate */

/* Register structure */
typedef struct Register
{
	/* Main section */
	uint8_t null;
	uint8_t cmd;

	/* Settings section */
	uint8_t hkperiod;
	union
	{
		struct
		{
			uint8_t temp :1; /* bit 0 */
			uint8_t current :1;
			uint8_t volt :1;
			uint8_t batt_status :1;
			uint8_t unused :4;
		};
		uint8_t byte;
	} measure;
	
	/* Flags section */
	union
	{
		struct
		{
			uint8_t busy :1;
			uint8_t complete :1;
			uint8_t charging :1;
			uint8_t full :1;
			uint8_t charging_enabled :1;
			uint8_t unused :3;
		};
		uint8_t byte;
	} flag;

	/* Values section */
	uint8_t mcutemp_l;
	uint8_t mcutemp_h;
	uint8_t c1_l;
	uint8_t c1_h;
    uint8_t c2_l;
    uint8_t c2_h;
    uint8_t c3_l;
    uint8_t c3_h;
    uint8_t c4_l;
    uint8_t c4_h;    
    uint8_t c5_l;
    uint8_t c5_h;
    uint8_t c6_l;
    uint8_t c6_h;    
    uint8_t c7_l;
    uint8_t c7_h;
    uint8_t c8_l;
    uint8_t c8_h;  
    uint8_t c9_l;
    uint8_t c9_h;
    uint8_t c10_l;
    uint8_t c10_h;   
    uint8_t c11_l;
    uint8_t c11_h;
    uint8_t c12_l;
    uint8_t c12_h; 
	uint8_t v1_l;
	uint8_t v1_h;
    uint8_t v2_l;
    uint8_t v2_h;
    uint8_t v3_l;
    uint8_t v3_h;
} Register_t;

/* Register numbering (only needed for writable registers) */
#define REG_CMD					1
#define REG_HKPERIOD			2
#define REG_MEASURE				3

/* Command OpCodes */
#define CMD_RESET				0x01
#define CMD_MEASURE				0x02
#define	CMD_CHARGING_ENABLE		0x03
#define CMD_CHARGING_DISABLE	0x04

#endif
